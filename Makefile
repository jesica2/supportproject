build-all: 
	docker-compose build

build-app: 
	docker-compose build app

build-api: 
	docker-compose build api

start-all:
	docker-compose up  

start-app:
	docker-compose up app 

start-api:
	docker-compose up api 

unit-app:
	docker-compose run --rm app npm run test $(ARGS)

unit-one:
	docker-compose run --rm app npm run test -- --include **/$(ARGS).spec.ts

unit-api:
	docker-compose run api sh -c "python manage.py test"

unit-all:
	docker-compose run --rm app npm run test
	docker-compose run api sh -c "python manage.py test"
	
down:
	docker-compose down