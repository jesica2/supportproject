import Salute from "../components/salute.vue"
import Greets from "../services/greets";
import { render } from "@testing-library/vue"

describe("SaluteComponent", () => {
  it("renders a Hello World", async () => {
    renderComponent()

    const greets: Greets = new Greets()

    const genericGreet = await greets.genericSalute()

    expect(genericGreet).toBe("Hello World")
  })
})

function renderComponent() {
  render(Salute, {
    props: {
        salute: "",
      },
  },
 
  )}
