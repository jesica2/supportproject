import axios from "axios"

export default class Greet{
    url: string
    personalSalutePath:string
    constructor() {
      this.url = "http://localhost:8000/"
      this.personalSalutePath = "http://localhost:8000/greet/"
    }
  
    async genericSalute() {
      try{
        const response = await axios.get(this.url)
      
        const salute = response.data
        
        return salute
          
      }catch(err){
        console.log(err)
      }
     
    }
    
    async personalSalute(name:string) {
      const payload = {
        name: name,
      }
      const response = await axios.post(this.personalSalutePath, payload)
      const salute = response.data.salute
  
      return salute
    }
  }