declare module "*.vue" {
    import Vue from "vue"
    export default Vue
  }
  
  /* This file helps your IDE to understand what a file ending in .vue is */