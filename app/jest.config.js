const { pathsToModuleNameMapper } = require("ts-jest"); // eslint-disable-line
const { compilerOptions } = require("./tsconfig"); // eslint-disable-line

module.exports = {
  preset: "ts-jest",
  roots: ["<rootDir>/src/tests/"],
  testMatch: ["**/+(*.)+(spec).+(ts)"],
  setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
  transform: {
    "^.+\\.vue$": "vue3-jest",
    "^.+\\.(ts|tsx)$": "ts-jest",
  },
  moduleFileExtensions: ["js", "ts", "vue"],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths || {}, {
    prefix: "<rootDir>/src/",
  }),
  testEnvironment: "jsdom",
}