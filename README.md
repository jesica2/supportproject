# Suport Project

## Description

This is a learning project of BeLike Academy. This project is built with nodejs, express, mongoDB, vue3 and applying TDD with jest.

## Installation

In the root of the project:
make build-all

## Usage

make start-all

Open http://localhost:3000 to view in the browser

## test

make unit-all

## Authors and acknowledgment

@jesica2
